import unittest
from rpc import trouverGagnant

class TestTrouverGagnant(unittest.TestCase):
    def test_egalite(self):
        self.assertEqual(trouverGagnant('roche', 'roche'), 'C\'est égal. Recommence!')
        self.assertEqual(trouverGagnant('papier', 'papier'), 'C\'est égal. Recommence!')
        self.assertEqual(trouverGagnant('ciseaux', 'ciseaux'), 'C\'est égal. Recommence!')

    def test_humain_gagne(self):
        self.assertTrue(trouverGagnant('roche', 'ciseaux'))
        self.assertTrue(trouverGagnant('papier', 'roche'))
        self.assertTrue(trouverGagnant('ciseaux', 'papier'))

    def test_ordi_gagne(self):
        self.assertFalse(trouverGagnant('roche', 'papier'))
        self.assertFalse(trouverGagnant('papier', 'ciseaux'))
        self.assertFalse(trouverGagnant('ciseaux', 'roche'))

if __name__ == '__main__':
    unittest.main()
